<?php

include_once 'hewan/Elang.php';
include_once 'hewan/Harimau.php';

$elang = new Elang;
$harimau = new Harimau;

// Menampilkan Info Hewan
echo $elang->getInfoHewan();
echo '<br><br>';
echo $harimau->getInfoHewan();
echo '<br><br>';

// Menampilkan Atraksi Hewan
echo $elang->atraksi();
echo '<br>';
echo $harimau->atraksi();

// Elang serang Harimau
echo '<br><br>';
echo $elang->serang($harimau) . '<br>';
echo 'Darah elang1 = '.$elang->darah.' <br>';
echo 'Darah harimau1 = '.$harimau->darah.' <br>';

// Harimau serang Elang 
echo '<br>';
echo $harimau->serang($elang) . '<br>';
echo 'Darah elang1 = '.$elang->darah.' <br>';
echo 'Darah harimau1 = '.$harimau->darah.' <br>';
